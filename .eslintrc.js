module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ['plugin:vue/essential', 'eslint:recommended'],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-undef': 'off',
    'prefer-rest-params': 'off',
    'prefer-spread': 'off',
    'no-unreachable': 'off',
    'no-extra-boolean-cast': 0,
    'no-unused-vars': 'off',
    'no-this-alias': ['off', { allowedNames: ['self'] }],
    quotes: [1, 'single'],
    'vue/no-use-v-if-with-v-for': 'off',
    'vue/no-unused-components': 'off',
    'no-extra-semi': 'off',
    'vue/no-side-effects-in-computed-properties': 'off'
  }
}

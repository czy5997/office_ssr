const fs = require('fs')
const path = require('path')
const Router = require('koa-router')
const send = require('koa-send')
const router = new Router()
const jsdom = require('jsdom')
const { JSDOM } = jsdom
const resolve = file => path.resolve(__dirname, file)
/* 模拟window对象逻辑 */
const resourceLoader = new jsdom.ResourceLoader({
  userAgent:
    'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'
}) // 设置UA
const dom = new JSDOM('', {
  url: 'https://app.nihaoshijie.com.cn/index.html',
  resources: resourceLoader
})

global.window = dom.window
global.document = window.document
global.navigator = window.navigator
window.nodeis = true //给window标识出node环境的标志位
/* 模拟window对象逻辑 */
// 第 2 步：获得一个createBundleRenderer
const { createBundleRenderer } = require('vue-server-renderer')
const bundle = require('../dist/vue-ssr-server-bundle.json')
const clientManifest = require('../dist/vue-ssr-client-manifest.json')

const renderer = createBundleRenderer(bundle, {
  runInNewContext: false,
  template: fs.readFileSync(resolve('../src/index.temp.html'), 'utf-8'),
  clientManifest: clientManifest
})

function renderToString(context) {
  return new Promise((resolve, reject) => {
    renderer.renderToString(context, (err, html) => {
      console.log(context.meta.inject())
      // console.log(html)

      const { title, meta } = context.meta.inject()
      html = html.replace(/<title[^>]*>[\s\S]*?<\/[^>]*title>/gi, title.text())
      html = html.replace(
        /<meta name="(keywords|description)"(.*?)\/?>/gi,
        meta.text()
      )
      // console.log(html)

      err ? reject(err) : resolve(html)
    })
  })
}

// 第 3 步：添加一个中间件来处理所有请求
const handleRequest = async (ctx, next) => {
  // console.log(ctx)
  const url = ctx.path
  if (url.includes('http')) {
    console.log(ctx)
  }
  if (url.includes('.')) {
    //  console.log(`proxy ${url}`)
    return await send(ctx, url, { root: path.resolve(__dirname, '../dist') })
  }
  // else {
  //   console.log('url', url)
  // }
  ctx.res.setHeader('Content-Type', 'text/html')

  const context = {
    title: '全国建筑企业资质查询-施工资质办理服务_小牛建讯官网',
    url
  }
  // console.log(context)
  // renderer.renderToString(context, (err, html) => {
  //   const { title } = context.meta.inject()
  //   console.log(title)
  //   context.title = title.text()
  // })
  // // 将 context 数据渲染为 HTML
  // console.log(context)
  const html = await renderToString(context)
  ctx.body = html
}
router.get('*', handleRequest)
module.exports = router

const VueSSRServerPlugin = require('vue-server-renderer/server-plugin')
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin')
const nodeExternals = require('webpack-node-externals')
const CompressionWebpackPlugin = require('compression-webpack-plugin')

const merge = require('lodash.merge')
const TARGET_NODE = process.env.WEBPACK_TARGET === 'node'
const target = TARGET_NODE ? 'server' : 'client'
const isDev = process.env.NODE_ENV !== 'production'
let path = '/'
if (process.env.VUE_APP_TYPE === 'test') {
  // 本地测试
  path = 'http://192.168.0.108:3000'
} else if (process.env.VUE_APP_TYPE === 'preview') {
  // 线上测试
  path = 'http://www.xnjx666.com:3000'
} else if (process.env.VUE_APP_TYPE === 'online') {
  path = 'http://www.xnjx666.com:80'
} // 正式环境
module.exports = {
  publicPath: path,
  // 'http://192.168.0.108:3000',
  // 'http://www.xnjx666.com:80', // 正式环境
  // 'http://127.0.0.1:3000',
  devServer: {
    historyApiFallback: true,
    headers: { 'Access-Control-Allow-Origin': '*' }
  },
  css: {
    extract: process.env.NODE_ENV === 'production',
    loaderOptions: {
      less: {
        lessOptions: {
          modifyVars: {
            'primary-color': '#00AB94',
            'link-color': '#00AB94',
            'border-radius-base': '2px',
            'heading-color': '#282828',
            'text-color': '#686868',
            'text-color-secondary': '#A0A6AB',
            'input-placeholder-color': '#A0A6AB',
            'disabled-color': '#A0A6AB',
            'disabled-bg': '#F2F6F8',
            'border-color-base': '#EFF3F6',
            // 'select-border-color': '#D2D6D9',
            // 'btn-default-borde': '#D2D6D9',
            'dropdown-selected-color': '#F2F6F8',
            'select-item-selected-bg': '#F2F6F8',
            'select-item-active-bg': '#F2F6F8',
            'item-hover-bg': '#F2F6F8',
            'table-header-bg': '#F2F6F8',
            'border-color-split': '#F2F6F8',
            'btn-disable-bg': '#D2D6D9',
            'btn-disable-color': '#fffff'
            // 'table-row-hover-bg':'#F2F6F8'
          },
          javascriptEnabled: true
        }
      }
    }
  },
  configureWebpack: () => ({
    // 将 entry 指向应用程序的 server / client 文件
    entry: `./src/entry-${target}.js`,
    // 对 bundle renderer 提供 source map 支持
    devtool: 'source-map',
    target: TARGET_NODE ? 'node' : 'web',
    node: TARGET_NODE ? undefined : false,
    output: {
      libraryTarget: TARGET_NODE ? 'commonjs2' : undefined
    },
    // https://webpack.js.org/configuration/externals/#function
    // https://github.com/liady/webpack-node-externals
    // 外置化应用程序依赖模块。可以使服务器构建速度更快，
    // 并生成较小的 bundle 文件。
    externals: TARGET_NODE
      ? nodeExternals({
          // 不要外置化 webpack 需要处理的依赖模块。
          // 你可以在这里添加更多的文件类型。例如，未处理 *.vue 原始文件，
          // 你还应该将修改 `global`（例如 polyfill）的依赖模块列入白名单
          whitelist: [/\.css$/],
          modulesFromFile: { include: ['vue-style-loader'] }
        })
      : undefined,
    optimization: {
      splitChunks: TARGET_NODE
        ? false
        : {
            chunks: 'all',
            maxInitialRequests: 10,
            minSize: 30000,
            cacheGroups: {
              vendor: {
                test: /[\\/]node_modules[\\/]/,
                name(module) {
                  // get the name. E.g. node_modules/packageName/not/this/part.js
                  // or node_modules/packageName
                  const packageName = module.context.match(
                    /[\\/]node_modules[\\/](.*?)([\\/]|$)/
                  )[1]
                  // npm package names are URL-safe, but some servers don't like @ symbols
                  return `npm.${packageName.replace('@', '')}`
                }
              }
            }
          }
    },
    plugins: [
      TARGET_NODE ? new VueSSRServerPlugin() : new VueSSRClientPlugin(),
      new CompressionWebpackPlugin({
        algorithm: 'gzip',
        test: /\.js$|\.html$|\.json$|\.css/,
        threshold: 10240,
        minRatio: 0.8
      })
    ]
  }),
  chainWebpack: config => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .tap(options => {
        return merge(options, {
          optimizeSSR: false
        })
      })

    // fix ssr hot update bug
    if (TARGET_NODE) {
      config.plugins.delete('hmr')
    }
  }
}

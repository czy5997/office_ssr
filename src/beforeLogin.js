// import router from './router/index'
import Cookie from 'js-cookie'
import { message, Modal } from 'ant-design-vue'
import httpRequestor from './utils/http'
import { createStore } from './store'

// import { userModule } from './store/modules/user'
export default function beforeLogin(to, from, next) {
  const Store = createStore()
  const whiteList = [
    'Login',
    'Home',
    'dynamic',
    'caseCenter',
    'aboutUs',
    'contactUs',
    'Download',
    'Vip',
    'details',
    'Product'
  ]

  function getUserInfo(next, to, from) {
    const userId = Cookie.get('userId')
    httpRequestor
      .get('member/get', { id: userId })
      .then(r => {
        Store.commit('setUserInfo', r)
        next()
      })
      .catch(err => {
        Cookie.remove('userId')
        if (err && location.hash.indexOf('login') < 0) {
          router.push('/Home')
        } else {
          router.replace('/Login')
        }
      })
  }

  // router.beforeEach((to, from, next) => {
  Modal.destroyAll()
  const haveUserId = Cookie.get('userId')
  const sendLogoInfo = Cookie.get('isLogoIn')
  if (!haveUserId) {
    // 未登录
    Cookie.remove('isLogoIn')
    if (!whiteList.find(e => e === to.name)) {
      message.warning('您还未登录，请先登录')
      setTimeout(() => {
        Store.commit('setShowLoginModal', true)
        // next(`/login?from=${to.name}`)
      }, 1000)
    } else {
      next()
    }
  } else {
    if (!sendLogoInfo) {
      const analyze = function() {
        const w = window
        const n = '_qha'
        w[n] =
          typeof w[n] === 'function'
            ? w[n]
            : function() {
                ;(w[n].c = w[n].c || []).push(arguments)
              }

        _qha('send', {
          et: 31,
          order: [
            {
              id: haveUserId /* 注册id, 必填项*/,
              orderType: '1' /* 常量，请勿修改*/
            }
          ]
        })
      }
      analyze()
      Cookie.set('isLogoIn', '已经登录')
    }
    getUserInfo(next, to, from)
  }
  // })
}
